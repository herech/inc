-- fsm.vhd: Finite State Machine
-- Author(s): Jan Herec, xherec00@stud.fit.vutbr.cz
--
library ieee;
use ieee.std_logic_1164.all;
-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
    CLK         : in  std_logic;
    RESET       : in  std_logic;

    -- Input signals
    KEY         : in  std_logic_vector(15 downto 0);
    CNT_OF      : in  std_logic;

    -- Output signals
    FSM_CNT_CE  : out std_logic;
    FSM_MX_MEM  : out std_logic;
    FSM_MX_LCD  : out std_logic;
    FSM_LCD_WR  : out std_logic;
    FSM_LCD_CLR : out std_logic
);
end entity fsm;

-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
    type t_state is (TEST_1, TEST_2, TEST_3, TEST_4_CODE_1, TEST_4_CODE_2, TEST_5_CODE_1, TEST_5_CODE_2, TEST_6_CODE_1, TEST_6_CODE_2, TEST_7_CODE_1, TEST_7_CODE_2, 
					 TEST_8_CODE_1, TEST_8_CODE_2, TEST_9_CODE_1, TEST_9_CODE_2, TEST_10_CODE_1, TEST_10_CODE_2, TEST_11,
					 INCORRECT_CODE, ACCESS_GRANTED, ACCESS_DENIED, FINISH);
    signal present_state, next_state : t_state;

begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
    if (RESET = '1') then
		present_state <= TEST_1;
    elsif (CLK'event AND CLK = '1') then
		present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
next_state_logic : process(present_state, KEY, CNT_OF)
begin
    case (present_state) is
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- TEST_1 predstavuje pocatecni stav, kdy testujeme prvni vstup klavesnice, napr. TEST_2 predstavuje stav, kdy testujeme druhy vstup klavesnice, atd.
    when TEST_1 =>
		next_state <= TEST_1;
		-- Overujeme, jestli byla stlacena klavesa 1, jelikoz oba moje kody maji stejnou pocatecni cislici, je vstup validni jen pro klavesu 1
		if (KEY(1) = '1') then
			next_state <= TEST_2;
		-- Pokud nebyla stlacena klavesa 1, ale bylo stlaceno neco jineho, je potreba overit, co to vlastne bylo 
		-- tento usek kodu je spolecny pro vsechny ostatni stavy, ve kterych testujeme vstupy a proto jej uz nebudu dale v kodu komentovat
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			-- Pokud byla stlacena klavesa #, chce uzivatel potvrdit vstup, ale jelikoz je vstup neplatny, 
			-- tak prejdeme do stavu ACCESS_DENIED, kde toto uzivateli sdelime
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			-- Pokud uzivatel stlacil nejakou jinou klavesu, tak prejdeme do stavu INCORRECT_CODE, 
			-- kde budeme cyklit, dokud uzivatel nepotvrdi jeho zadany kod klavesou #
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_2 =>
		next_state <= TEST_2;
		-- Overujeme, jestli byla stlacena klavesa 5, jelikoz oba moje kody maji stejnou druhou cislici, je vstup validni jen pro klavesu 5
		if (KEY(5) = '1') then
			next_state <= TEST_3;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_3 =>
		next_state <= TEST_3;
		-- Overujeme, jestli byla stlacena klavesa 3, jelikoz se moje dva kody od treti cislice (vcetne) lisi, je klavesa 3 validni jen pro kod 1
		-- Pokud podminka plati, prejdeme do stavu TEST_4_CODE_1, ktery je prvnim uzlem ve vetve, kde budeme testovat vstupy klavesnice vylucne pro kod 1
		if (KEY(3) = '1') then
			next_state <= TEST_4_CODE_1;
		-- Overujeme, jestli byla stlacena klavesa 4, jelikoz se moje dva kody od treti cislice (vcetne) lisi, je klavesa 4 validni jen pro kod 2
		-- Pokud podminka plati, prejdeme do stavu TEST_4_CODE_2, ktery je prvnim uzlem ve vetve, kde budeme testovat vstupy klavesnice vylucne pro kod 2
		elsif (KEY(4) = '1') then
			next_state <= TEST_4_CODE_2;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- TEST_4_CODE_1 je stav, ve kterem budeme testovat ctvrty vstup klavesnice a spravnost vstupu budeme overovat jen pro pro kod 1
	-- Pocinaje timto stavem testujeme vstupy uz jen pro kod 1
	when TEST_4_CODE_1 =>
		next_state <= TEST_4_CODE_1;
		if (KEY(3) = '1') then
			next_state <= TEST_5_CODE_1;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- TEST_4_CODE_2 je stav, ve kterem budeme testovat ctvrty vstup klavesnice a spravnost vstupu budeme overovat jen pro pro kod 2
	-- Pocinaje timto stavem testujeme vstupy uz jen pro kod 2
	when TEST_4_CODE_2 =>
		next_state <= TEST_4_CODE_2;
		if (KEY(6) = '1') then
			next_state <= TEST_5_CODE_2;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_5_CODE_1 =>
		next_state <= TEST_5_CODE_1;
		if (KEY(4) = '1') then
			next_state <= TEST_6_CODE_1;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_5_CODE_2 =>
		next_state <= TEST_5_CODE_2;
		if (KEY(4) = '1') then
			next_state <= TEST_6_CODE_2;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_6_CODE_1 =>
		next_state <= TEST_6_CODE_1;
		if (KEY(5) = '1') then
			next_state <= TEST_7_CODE_1;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_6_CODE_2 =>
		next_state <= TEST_6_CODE_2;
		if (KEY(9) = '1') then
			next_state <= TEST_7_CODE_2;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_7_CODE_1 =>
		next_state <= TEST_7_CODE_1;
		if (KEY(8) = '1') then
			next_state <= TEST_8_CODE_1;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_7_CODE_2 =>
		next_state <= TEST_7_CODE_2;
		if (KEY(2) = '1') then
			next_state <= TEST_8_CODE_2;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_8_CODE_1 =>
		next_state <= TEST_8_CODE_1;
		if (KEY(9) = '1') then
			next_state <= TEST_9_CODE_1;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_8_CODE_2 =>
		next_state <= TEST_8_CODE_2;
		if (KEY(7) = '1') then
			next_state <= TEST_9_CODE_2;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_9_CODE_1 =>
		next_state <= TEST_9_CODE_1;
		if (KEY(8) = '1') then
			next_state <= TEST_10_CODE_1;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_9_CODE_2 =>
		next_state <= TEST_9_CODE_2;
		if (KEY(0) = '1') then
			next_state <= TEST_10_CODE_2;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_10_CODE_1 =>
		next_state <= TEST_10_CODE_1;
		if (KEY(6) = '1') then
			next_state <= TEST_11;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	when TEST_10_CODE_2 =>
		next_state <= TEST_10_CODE_2;
		if (KEY(9) = '1') then
			next_state <= TEST_11;
		elsif (KEY(15 downto 0) /= "0000000000000000") then
			if (KEY(15) = '1') then
				next_state <= ACCESS_DENIED;
			else
				next_state <= INCORRECT_CODE;
			end if;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- TEST_11 je stavem, ktery nasleduje po spravne zadanem kodu 1 nebo 2, a v tomto stavu cekame na dalsi vstup
	when TEST_11 =>
		next_state <= TEST_11;
		-- Pokud bude dalsim vstupem klavesa #, uzivatel potvrdil kod a nas automat zaridi vypsani spravy "Pristup povolen"
		if (KEY(15) = '1') then
			next_state <= ACCESS_GRANTED;
		-- Pokud bude dalsim vstupem jinak klavesa, kod uz neni validni a prejdeme do stavu INCORRECT_CODE, 
		-- kde budeme cyklit, dokud uzivatel nepotvrdi jeho zadany kod klavesou #
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= INCORRECT_CODE;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- Ve stavu ACCESS_GRANTED vypiseme zpravu "Pristup povolen" a prejdeme do stavu FINISH
	when ACCESS_GRANTED =>
		next_state <= ACCESS_GRANTED;
		if (CNT_OF = '1') then
			next_state <= FINISH;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- Ve stavu ACCESS_DENIED vypiseme zpravu "Pristup odepren" a prejdeme do stavu FINISH
	when ACCESS_DENIED =>
		next_state <= ACCESS_DENIED;
		if (CNT_OF = '1') then
			next_state <= FINISH;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- Ve stavu INCORRECT_CODE jsme, protoze uzivatel zadal nespravny kod a cyklime zde tak dlouho, dokud uzivatel nepotvrdi zadany kod
	when INCORRECT_CODE =>
		next_state <= INCORRECT_CODE;
		if (KEY(15) = '1') then
			next_state <= ACCESS_DENIED;
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- Ve stavu FINISH uz jen cekame na stisk klavesy #, ktera zaridi vymazani displeje a prechod do pocatecniho stavu TEST_1
    when FINISH =>
		next_state <= FINISH;
		if (KEY(15) = '1') then
			next_state <= TEST_1; 
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- Pri nejakem nedefinovanem stavu prejdeme do pocatecniho stavu, cimz zvysime robusnost naseho automatu
    when others =>
		next_state <= TEST_1;
    end case;
end process next_state_logic;

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
    case (present_state) is
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- Ve stavu ACCESS_GRANTED nastavime vystupni signaly, tak aby mohla byt vypsana zprava "Pristup povolen"
    when ACCESS_GRANTED =>
		FSM_CNT_CE     <= '1';
		FSM_MX_MEM     <= '1';
		FSM_MX_LCD     <= '1';
		
		-- Dokud se nevyctou vsechny znaky na display, budeme cyklit v tomto stavu a postupne vypisovat jednotlive znaky
		if (CNT_OF = '0') then
			FSM_LCD_WR     <= '1';
			FSM_LCD_CLR    <= '0';
		-- Az jsou vsechny znaky vyctene, tak uz s displejem nic neprovadime
		else
			FSM_LCD_WR     <= '0';
			FSM_LCD_CLR    <= '0';
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- Ve stavu ACCESS_DENIED nastavime vystupni signaly, tak aby mohla byt vypsana zprava "Pristup odepren"
	when ACCESS_DENIED =>
		FSM_CNT_CE     <= '1';
		FSM_MX_MEM     <= '0';
		FSM_MX_LCD     <= '1';
		
		-- Dokud se nevyctou vsechny znaky na display, budeme cyklit v tomto stavu a postupne vypisovat jednotlive znaky
		if (CNT_OF = '0') then
			FSM_LCD_WR     <= '1';
			FSM_LCD_CLR    <= '0';
		-- Az jsou vsechny znaky vyctene, tak uz s displejem nic neprovadime
		else
			FSM_LCD_WR     <= '0';
			FSM_LCD_CLR    <= '0';
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
    when FINISH =>
		FSM_CNT_CE     <= '0';
		FSM_MX_MEM     <= '0';
		FSM_MX_LCD     <= '0';
		
		-- Ve stavu FINISH a za podminky, ze uzivatel stiskl klavesu #, nastavime vystupni signaly, tak aby mohl byt vymazan displej
		if (KEY(15) = '1') then
			FSM_LCD_WR     <= '0';
			FSM_LCD_CLR    <= '1';
		-- Pokud byla stisknuta jina klavesa, tak s displejem nic neprovadime
		else
			FSM_LCD_WR     <= '0';
			FSM_LCD_CLR    <= '0';
		end if;
    -- - - - - - - - - - - - - - - - - - - - - - -
	-- V jinach stavech
    when others =>
	    FSM_CNT_CE     <= '0';
		FSM_MX_MEM     <= '0';
		FSM_MX_LCD     <= '0';
		
		-- Za podminky, ze byla stisknuta klavesa jina nez #, nastavime vystupni signaly, tak, aby mohl byt na na displej zapsan znak *
		if (KEY(14 downto 0) /= "000000000000000") then
			FSM_LCD_WR     <= '1';
			FSM_LCD_CLR    <= '0';
		-- Za podminky, ze byla stisknuta klavesa #, nastavime vystupni signaly, tak aby mohl byt vymazan displej
		elsif (KEY(15) = '1') then
			FSM_LCD_WR     <= '0';
			FSM_LCD_CLR    <= '1';
		-- Pokud nebyla stisknuta zadna klavesa, tak s displejem nic neprovadime	
		else
			FSM_LCD_WR     <= '0';
			FSM_LCD_CLR    <= '0';
		end if;
    end case;
end process output_logic;

end architecture behavioral;

